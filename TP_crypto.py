import tensorflow as tf
import numpy as np
import datetime

text_size = 16
key_size = 16
batch_size = 10

# Function to create models 
def model(name)
    
    if (name == 'Eve')
        model = tf.keras.Sequential([
           
            tf.keras.layers.Dense(text_size, activation='tanh', input_shape=(16,)),
            tf.keras.layers.Reshape((16,1)),
            tf.keras.layers.Conv1D(2, 4, 1, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(4, 2, 2, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(4, 1, 1, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(1, 1, 1, padding='same', activation='sigmoid')
        ], name)
    else
        model = tf.keras.Sequential([
          
            tf.keras.layers.Dense(text_size + key_size, activation='tanh', input_shape=(32,)),
            tf.keras.layers.Reshape((32,1)),
            tf.keras.layers.Conv1D(2, 4, 1, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(4, 2, 2, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(4, 1, 1, padding='same', activation='tanh'),
            tf.keras.layers.Conv1D(1, 1, 1, padding='same', activation='sigmoid')
        ], name)

    return model

alice = model('Alice')
alice.summary()
bob = model('Bob')
bob.summary()
eve = model('Eve')
eve.summary()

EPOCHS = 500
optimizer = tf.keras.optimizers.Adam(learning_rate=0.01)

def eve_loss(input_message, eve_out_message, batch_size)
    # Mapping -1 to 0 and 1 to 1
    eve_diff = (eve_out_message + 1.0)2.0 - (input_message + 1.0)2.0
    
    # Eve's average L1 distance Loss of the given batch
    loss = (1batch_size)tf.reduce_sum(tf.abs(eve_diff))
    
    return loss

def alice_bob_loss(input_message, bob_out_message, eves_loss, batch_size)
    ## Alice-Bob Loss part-1
  
    # Mapping -1 to 0 and 1 to 1
    ab_diff = (bob_out_message + 1.0)2.0 - (input_message + 1.0)2.0
    
    # Alice and Bob's average L1 distance Loss of the given batch
    bob_reconstruction_loss = (1batch_size)tf.reduce_sum(tf.abs(ab_diff))

    
    ## Alice-Bob Loss part-2
    # To make sure Eve at least has 50% of bits wrong so that output simulates random probability of binary output
    # ((N2 - EveLoss)^2)((N2)^2)
    eve_evadropping_loss = tf.reduce_sum(tf.square(float(text_size)  2.0 - eves_loss)  (text_size  2)2)

    # bobs final reconstruction loss
    loss = bob_reconstruction_loss + eve_evadropping_loss
    
    return loss

def random_bools(size, n)
  
    # create a batch size of 'size' with 'n' number of bits per sample
    temp =  np.random.random_integers(0, high=2, size=[1, 16])

    # Convert 0 - -1 and 1 - 1 
    temp = temp2 - 1

    return np.array(temp, dtype=np.float32)
  
def get_dataset(sample_size, TEXT_SIZE, KEY_SIZE)

    m = random_bools(sample_size, TEXT_SIZE)
    k = random_bools(sample_size, KEY_SIZE)

    return m, k

#sample_size = 40965
#steps_per_epoch = int(sample_sizebatch_size)

# DATASET 
messages, keys = get_dataset(1, text_size, key_size)
current_time = datetime.datetime.now().strftime(%Y%m%d-%H%M%S)
summary_writer = tf.summary.create_file_writer('logstrain' + current_time)

for epoch in range(EPOCHS)
    with tf.GradientTape(persistent=True) as tape
        inputs = np.concatenate((messages, keys), axis=1)
        aliceout = alice(inputs)
        alice_out_key = np.concatenate((aliceout.numpy()[0].transpose(), keys), axis=1)
        bobout = bob(alice_out_key)
        eveout = eve(aliceout.numpy()[0].transpose())

        eveloss = eve_loss(messages, eveout, batch_size)
        bobloss = alice_bob_loss(messages, bobout, eveloss, batch_size)
        aliceloss = alice_bob_loss(messages, aliceout, eveloss, batch_size)

    bobgrads = tape.gradient(bobloss, bob.trainable_variables)
    alicegrads = tape.gradient(aliceloss, alice.trainable_variables)
    evegrads = tape.gradient(eveloss, eve.trainable_variables)

    optimizer.apply_gradients(zip(bobgrads, bob.trainable_variables))
    optimizer.apply_gradients(zip(alicegrads, alice.trainable_variables))
    optimizer.apply_gradients(zip(evegrads, eve.trainable_variables))

    with summary_writer.as_default()
        tf.summary.scalar('Bob's loss', bobloss, step=epoch)
        tf.summary.scalar('Alice's loss', aliceloss, step=epoch)
        tf.summary.scalar('Eve's loss', eveloss, step=epoch
